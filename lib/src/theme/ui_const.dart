import 'package:flutter/material.dart';

Color mainColor     = const Color.fromRGBO(114, 35, 186, 1.0); 
Color mainDarkColor = const Color.fromRGBO(03, 03, 03, 1.0);
Color bgColor       = const Color.fromRGBO(33, 33, 33, 1.0);